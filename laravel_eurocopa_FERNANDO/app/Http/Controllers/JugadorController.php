<?php

namespace App\Http\Controllers;
use App\Models\Jugador;
use Illuminate\Http\Request;

class JugadorController extends Controller
{
    public function buscar(Request $request){
        $texto = $request->nombre;
        $jugadores = Jugador::select('nombre')->where('numeroCamiseta',"$texto")->pluck('nombre');
        return $jugadores;
    }
}
