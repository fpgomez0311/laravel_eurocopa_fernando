<?php

namespace App\Http\Controllers;
use App\Models\Partido;
use App\Models\Jugador;
use Illuminate\Http\Request;

class RestWebServiceController extends Controller
{
    public function resetear ($id){
        try {
            $partido = Partido::find($id);
            $partido->disputado = false;
            $partido->goles_pais1 = 0;
            $partido->goles_pais2 = 0;
            $nombre1 = $partido->paises()[0]->nombre;
            $nombre2 = $partido->paises()[1]->nombre;
            $partido->save();
            return response()->json(["mensaje" => "Partido " .$nombre1."-".$nombre2." reseteado."]);
        } catch (\Exception $e) {
            return response()->json(["mensaje" => "Error al resetear"]);
        }

    }
    public function mostrar($pais,$posicion){
        try {
            $jugadores = Jugador::query()->where("pais_id", $pais);
            return response()->json($jugadores);
        } catch (\Exception $e) {
            return response()->json(["mensaje" => "No se han encontrando jugadores."]);
        }
    }
}
