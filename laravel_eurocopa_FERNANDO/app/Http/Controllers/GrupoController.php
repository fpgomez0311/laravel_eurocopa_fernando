<?php

namespace App\Http\Controllers;
use App\Models\Grupo;
use Illuminate\Http\Request;

class GrupoController extends Controller
{
    public function index(){
        $grupos=Grupo::all();
        return view("grupos.index",compact("grupos"));
    }
}
