<?php

namespace App\Http\Controllers;

use App\Models\Pais;
use Illuminate\Http\Request;

class PaisController extends Controller
{
    public function show(Pais $pais){
        return view("paises.show", compact("pais"));
    }
}
