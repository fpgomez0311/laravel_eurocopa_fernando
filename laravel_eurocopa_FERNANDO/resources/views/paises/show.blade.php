@extends("layouts.master")

@section("titulo")
{{$pais->nombre}}
@endsection

@section("contenido")
<div class="container">
  
    <h1>{{$pais->nombre}}</h1>

    <h3>Partidos</h3>
    @foreach($pais->partidos as $partido)
    @if( $partido->disputado === 1 ) 
    <p>{{$partido->paises()[0]->nombre}} {{$partido->goles_pais1}} - {{$partido->paises()[1]->nombre}} {{$partido->goles_pais2}}</p>
    @else
    <p>{{$partido->paises()[0]->nombre}} - {{$partido->paises()[1]->nombre}} (NO DISPUTADO)</p>
    @endif
    @endforeach
    <h3>Jugadores</h3>
    <table>
    <tr>
    <td>Número</td>
    <td>Nombre</td>
    <td>Posición</td>
    <td>Edad</td>
    </tr>
    @foreach($pais->jugadores as $jugador)
    <tr>
    <td>{{$jugador->numeroCamiseta}}</td>
    <td>{{$jugador->nombre}}</td>
    <td>{{$jugador->posicion}}</td>
    <td>{{$jugador->getEdad()}}</td>
    </tr>
    @endforeach
  </table>
</div>
@endsection