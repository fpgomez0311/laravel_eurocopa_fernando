@extends("layouts.master")

@section("titulo")
Grupos
@endsection

@section("contenido")
<div class="container">
  
    @foreach( $grupos as $clave => $grupo)
     <h3>Grupo  {{$grupo->letra}}</h3>
     <ul>
                        @foreach($grupo->paises as $pais)
                            <li>{{$pais->nombre}} -  {{count($pais->partidos)}}</li>
                        @endforeach
                    </ul>
    @endforeach
  
</div>
@endsection