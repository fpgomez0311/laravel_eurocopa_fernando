<?php

namespace Database\Factories;

use App\Models\Jugador;
use App\Models\Pais;
use Illuminate\Database\Eloquent\Factories\Factory;

class JugadorFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Jugador::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $nombre = $this->faker->name('male');
        $numero = $this->faker->numberBetween(1,25);
        $fechaNac = $this->faker->dateTimeInInterval($startDate = '-35 years', $interval = '+ 18 years', $timezone = null);
        $posicion = $this->faker->randomElement(["POR","DEF","CEN","DEL"]);
        $idpais = Pais::all()->random()->id;
        return [
           'nombre' => $nombre,
           'numeroCamiseta' => $numero,
           'fechaNacimiento' => $fechaNac,
           'posicion' => $posicion,
           'pais_id' => $idpais
        ];
    }
}
