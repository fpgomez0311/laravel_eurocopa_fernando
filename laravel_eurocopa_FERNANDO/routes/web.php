<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\GrupoController;
use App\Http\Controllers\PaisController;
use App\Http\Controllers\JugadorController;
use App\Http\Controllers\RestWebServiceController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->action([GrupoController::class, 'index']);
});

Route::get('grupos', [GrupoController::class, 'index'])->name('grupos.index');

Route::get('paises/{pais}', [PaisController::class, 'show'])->name('paises.show');
Route::post('jugadores/busquedaAjax', [JugadorController::class,'buscar']);
Route::get("api/partido/{partido}/resetear", [RestWebServiceController::class, "resetear"])
->name("rest.resetear");

Route::get("api/jugadores/{pais}/{posicion}", [RestWebServiceController::class, "mostrar"])
->name("rest.mostrar");